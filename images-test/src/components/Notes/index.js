import React, { Component } from 'react';
import { Link } from 'react-router';
import classNames from 'classnames';
import { styles } from './styles.scss';

import Childs from '../Childs';


export default class Notes extends Component {
  render() {
    const { notes } = this.props;
    return (
      <section className={styles}>
        <Childs parentId={0} trees={notes} triggerOpen={this.props.triggerOpen}/>
      </section>
    );
  }
}
