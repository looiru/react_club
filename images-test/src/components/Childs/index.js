import React, { Component } from 'react';

import { Link } from 'react-router';
import { getChilds } from '../getChilds.js';

export default class Childs extends Component {
    _triggerOpen(id) {
      this.props.triggerOpen(id);
    }
    render() {
        const { trees, parentId } = this.props;
        const childTree = getChilds(trees, parentId);

        if(!childTree || childTree.size === 0) return( <div></div> );

        return(
            <ul>
                {
                    childTree.map((item, key) => {
                      const parentIdChild = item.get('id');
                      return (
                          <li key={key}>
                             <span
                               onClick={this._triggerOpen.bind(this, item.get('id'))}
                               >[{item.get('open') ? '+' : '-'}]&nbsp;</span>
                              <Link to={`/edit/${item.get('id')}`}>
                                {item.get('title')}
                              </Link>
                              {
                                item.get('open') ? <Childs parentId={parentIdChild} trees={trees} triggerOpen={this.props.triggerOpen}/> : <div></div>
                              }
                          </li>
                      );
                    })
                }
            </ul>
        );
    }
}
