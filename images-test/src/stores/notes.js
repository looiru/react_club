import { fromJS } from 'immutable';

export const UPDATE_TEXT = 'UPDATE_TEXT';
export const TRIGGER_OPEN = 'TRIGGER_OPEN';

const tree = [
  {
    id: 1,
    title: 'first',
    text: 'first',
    parentId: 0,
    open: true,
  },
  {
    id: 2,
    title: 'second',
    text: 'second',
    parentId: 1,
    open: false,

  },
  {
    id: 3,
    title: 'third',
    text: 'third',
    parentId: 2,
    open: true,
  },
  {
    id: 4,
    title: 'fourth',
    text: 'fourth',
    parentId: 0,
    open: true,
  },

];

const defaultState = fromJS(tree);


export default function (state = defaultState, action) {
  switch (action.type) {
    case UPDATE_TEXT:
      return state.map((item) => {
        if (item.get('id') == action.payload.id) {
          return item.set('text', action.payload.text);
        }
        return item;
      });
    case TRIGGER_OPEN:
      return state.map((item) => {
        if (item.get('id') == action.payload.id) {
          return item.set('open', !item.get('open'));
        }
        return item;
      });

    default:
      return state;
  }
}

export function updateText(id, text) {
  return {
    type: UPDATE_TEXT,
    payload: {text: text, id: id}
  }
}

export function triggerOpen(id) {

  return {
    type: TRIGGER_OPEN,
    payload: {id: id},
  }
}

