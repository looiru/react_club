import { connect } from 'react-redux';

import Notes from '../components/Notes';

// Actions
import {
  updateText,
  triggerOpen,
} from '../stores/notes.js';

function mapStateToProps(state) {
  return {
    notes: state.notes,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    updateText: (id, text) => dispatch(updateText(id, text)),
    triggerOpen: (id) => dispatch(triggerOpen(id))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Notes);
