import { connect } from 'react-redux';

import Editor from '../components/Editor';

// Actions
import {
  updateText
} from '../stores/notes.js';

function mapStateToProps(state) {
  return {
    notes: state.notes,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    updateText: (id, text) => dispatch(updateText(id, text))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Editor);
